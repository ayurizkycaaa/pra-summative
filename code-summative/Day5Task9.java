import java.util.HashMap;
import java.util.Map;

public class Day5Task9 {
    public static void main(String[] args) {
        HashMap<String, Integer> peserta = new HashMap<String, Integer>();
        peserta.put("Ayu", 5);
        peserta.put("Biagi", 12);
        peserta.put("Cipta", 15);
        peserta.put("Dovyan", 2);

        System.out.println("Pengumuman capaian peserta lari");
        for (Map.Entry<String, Integer> entry : peserta.entrySet()) {
            int jarak = entry.getValue();
            if (jarak >= 10 ) {
                System.out.println("Selamat " + entry.getKey() + " telah berlari sejauh" + entry.getValue() + " KM");
            } else {
                System.out.println("Tahun depan coba lagi ya.. " + entry.getKey());
            }
        }
    }  
}