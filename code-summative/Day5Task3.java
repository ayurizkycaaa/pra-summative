import java.util.Scanner;

public class Day5Task3 {
    
    public static void main(String[] args) {
        int pilih;
        double alas, tinggi, miring;
        System.out.println("MENGHITUNG LUAS DAN KELILING SEGITIGA SIKU SIKU");
        System.out.println("1. Menghitung Keliling");
        System.out.println("2. Menghitung Luas");
        System.out.print("Pilih Angka : ");
        try (Scanner keyboard = new Scanner(System.in)) {
            pilih = keyboard.nextInt();
        
        switch (pilih) {
            case 1:
                System.out.println("Menghitung Keliling, Masukkan Nilai (dalam meter)");
                System.out.print("Alas : ");
                alas = keyboard.nextInt();
                System.out.print("Tinggi : ");
                tinggi = keyboard.nextInt();
                System.out.print("Miring : ");
                miring = keyboard.nextInt();
                System.out.print("Keliling Segitiga : " + keliling(alas, miring, tinggi) + " meter"); 
                break;

            case 2:
                System.out.println("Menghitung Luas, Masukkan Nilai (dalam meter)");
                System.out.print("Alas : ");
                alas = keyboard.nextInt();
                System.out.print("Tinggi : ");
                tinggi = keyboard.nextInt();
                System.out.print("Luas Segitiga : " + luas(alas, tinggi) + " meter kubik"); 
                break;
            default:
                System.out.println("Masukkan angka yang benar");
                break;
        }
        }catch (Exception e) {
            System.out.println("Maaf, Tolong inputkan angka");
        }
    }
    static double keliling(double alas, double miring, double tinggi) {
        double keliling = alas + miring + tinggi;
        return keliling;
    }

    static double luas(double alas, double tinggi) {
        double luas = 0.5 * (alas + tinggi);
        return luas;
    }
}
