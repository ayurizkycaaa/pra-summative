public class Day5Task102 extends Mahasiswa{

    @Override
    public void kegiatan() {
        System.out.println("Kuliah dan Join Komunitas");
    }

    public static void main(String[] args) {
        Day5Task102 mahasiswa1 = new Day5Task102();
        mahasiswa1.nama = "ayu";
        mahasiswa1.umur = 23;
        System.out.println("Nama : " + mahasiswa1.nama);
        System.out.println("Umur : " + mahasiswa1.umur);
        System.out.print("Kegiatan : " );
        mahasiswa1.kegiatan();
    }
    
}
