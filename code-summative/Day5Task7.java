import java.util.Scanner;

public class Day5Task7 {
    public static void main(String[] args) {
        int[] angka = new int[5];
        System.out.println("Menjumlahkan 5 Deret Angka");
        try (Scanner keyboard = new Scanner(System.in)) {
            for (int i = 0; i < angka.length; i++) {
                System.out.print("Angka ke-" + (i + 1) + ": ");
                angka[i] = keyboard.nextInt();
            }
            System.out.println("---------------------------");
            int total = jumlahkanAngka(angka);
            System.out.println("Total angka array : " + total);
        }catch (Exception e) {
            System.out.println("Masukkan Angka yaa...");
        }
    }

    public static int jumlahkanAngka(int[] arrayAngka) {
        int jumlah = 0;
        for (int angka : arrayAngka) {
            jumlah += angka;
        }
        return jumlah;
    }

}
