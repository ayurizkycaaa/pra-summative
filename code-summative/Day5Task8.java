import java.util.ArrayList;
import java.util.Scanner;

public class Day5Task8 {
    public static void main(String[] args) {
        ArrayList<Integer> angka = new ArrayList<>();
        System.out.println("Menjumlahkan 5 Deret Angka");
        try (Scanner keyboard = new Scanner(System.in)) {
            for (int i = angka.size(); i < 5 ; i++) {
                System.out.print("Angka ke-" + (i+1) + " : ");
                angka.add(keyboard.nextInt());
            }
            System.out.println("---------------------------");
            int total = jumlahkanAngka(angka);
            System.out.println("Total 5 angka dalam list : " + total);
        }catch (Exception e) {
            System.out.println("Masukkan Angka yaa...");
        }
    }

    public static int jumlahkanAngka(ArrayList<Integer> listAngka) {
        int jumlah = 0;
        for (int angka : listAngka) {
            jumlah += angka;
        }
        return jumlah;
    }

}
