import java.util.Scanner;

public class Day5Task6 {
    public static void main(String[] args) {
        int jumlah;
        System.out.println("MENAMPILKAN SEGITIGA SIKU SIKU TERBALIK");
        System.out.print("Masukkan Angka Jumlah Perulangan : ");
        try (Scanner keyboard = new Scanner(System.in)) {
            jumlah = keyboard.nextInt();
            for (var i = jumlah; i > 0; i--) {
                if (i == 6) {
                    System.out.println("Baris ke-" + i + " ");
                } else if (i != 6) {
                    System.out.print("Baris ke-" + i + " ");
                    for (var j = 0; j < i; j++) {
                        System.out.print("*");
                    }
                    System.out.println();
                }
            }
        }
    }
}
