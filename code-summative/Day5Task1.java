public class Day5Task1 {
    private double berat;
    private double tinggi;

    private static double bmi = 24;

    public Day5Task1(double berat, double tinggi) {
        this.berat = berat;
        this.tinggi = tinggi;
    }

    public static void main(String[] args) {
        Day5Task1 ayu = new Day5Task1(52, 1.52);
        ayu.hitungBMI();
    }

    public void hitungBMI() {
        double BMI = (this.berat / (this.tinggi*this.tinggi));
        if (bmi < BMI) {
            System.out.println("kamu kelebihan berat badan");
        }
        System.out.println("Berat badan kamu sudah cukup ideal");
        System.out.print("BMI kamu : " + BMI );
        
    }

}
