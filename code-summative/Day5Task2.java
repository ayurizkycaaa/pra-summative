import java.util.Scanner;

public class Day5Task2 {

    public static void main(String[] args) {
        int angka;
        System.out.println("MENENTUKAN GANJIL GENAP");
        System.out.print("Masukkan Angka : ");
        try (Scanner keyboard = new Scanner(System.in)) {
            angka = keyboard.nextInt(); 
            int mod = angka%2;
            String ucapan = mod == 0 ? "Ini Angka Genap" : "Ini Angka Ganjil";
            System.out.println(ucapan);
        }catch (Exception e) {
            System.out.println("Maaf, inputan salah");
        }
    }
}
