public class Day5Task5 {
    public static void main(String[] args) {
        int jumlah = 6;
        System.out.println("MENAMPILKAN SEGITIGA SIKU SIKU");

        for (var i = jumlah; i >= 1; i--) {
            for (var j = 1; j <= jumlah; j++) {
                if (j >= i) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }
}
